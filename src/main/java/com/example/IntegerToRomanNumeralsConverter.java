package com.example;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntegerToRomanNumeralsConverter {

    public static final String ZERO = "";
    public static final String ONE = "I";
    public static final String FOUR = "IV";
    public static final String FIVE = "V";
    public static final String NINE = "IX";
    public static final String TEN = "X";
    public static final String FORTY = "XL";
    public static final String FIFTY = "L";
    public static final String NINETY = "XC";
    public static final String ONE_HUNDRED = "C";
    private static final String FOUR_HUNDRED = "CD";
    private static final String FIVE_HUNDRED = "D";
    private static final String ONE_THOUSAND = "M";
    private static final String NINE_HUNDRED = "CM";

    private static final List<Integer> BASE_NUMBERS = Arrays.asList(
            1000, 900, 500, 400, 100, 90, 50, 40, 10, 5, 1
    );

    private static final Map<Integer, String> BASE_CASES = new HashMap<>();

    static {
        BASE_CASES.put(0, ZERO);
        BASE_CASES.put(1, ONE);
        BASE_CASES.put(4, FOUR);
        BASE_CASES.put(5, FIVE);
        BASE_CASES.put(9, NINE);
        BASE_CASES.put(10, TEN);
        BASE_CASES.put(40, FORTY);
        BASE_CASES.put(50, FIFTY);
        BASE_CASES.put(90, NINETY);
        BASE_CASES.put(100, ONE_HUNDRED);
        BASE_CASES.put(400, FOUR_HUNDRED);
        BASE_CASES.put(500, FIVE_HUNDRED);
        BASE_CASES.put(900, NINE_HUNDRED);
        BASE_CASES.put(1000, ONE_THOUSAND);
    }

    public String toRoman(int i) {
        if (BASE_CASES.containsKey(i) ) {
            return BASE_CASES.get(i);
        }

        for(Integer baseNumber : BASE_NUMBERS) {
            if (i > baseNumber) {
                return toRoman(baseNumber) + toRoman(i - baseNumber);
            }
        }
        throw new IllegalArgumentException("Invalid input:" + i);
    }
}
