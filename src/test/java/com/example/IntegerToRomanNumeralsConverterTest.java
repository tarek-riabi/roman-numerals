package com.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class IntegerToRomanNumeralsConverterTest {

    private IntegerToRomanNumeralsConverter converter;

    @BeforeEach
    public void init() {
        converter = new IntegerToRomanNumeralsConverter();
    }
    
    @Test
    public void converter_returns_empty_given_0() {
        assertEquals("", converter.toRoman(0));
    }

    @Test
    public void converter_returns_I_given_1() {
        assertEquals("I", converter.toRoman(1));
    }

    @Test
    public void converter_returns_II_given_2() {
        assertEquals("II", converter.toRoman(2));
    }

    @Test
    public void converter_returns_III_given_3() {
        assertEquals("III", converter.toRoman(3));
    }

    @Test
    public void converter_returns_V_given_5() {
        assertEquals("V", converter.toRoman(5));
    }

    @Test
    public void converter_returns_VI_given_6() {
        assertEquals("VI", converter.toRoman(6));
    }

    @Test
    public void converter_returns_IV_given_4() {
        assertEquals("IV", converter.toRoman(4));
    }

    @Test
    public void converter_returns_VII_given_7() {
        assertEquals("VII", converter.toRoman(7));
    }

    @Test
    public void converter_returns_VIII_given_8() {
        assertEquals("VIII", converter.toRoman(8));
    }

    @Test
    public void converter_returns_IX_given_9() {
        assertEquals("IX", converter.toRoman(9));
    }

    @Test
    public void converter_returns_X_given_10() {
        assertEquals("X", converter.toRoman(10));
    }

    @Test
    public void converter_returns_XI_given_11() {
        assertEquals("XI", converter.toRoman(11));
    }

    @Test
    public void converter_returns_XII_given_12() {
        assertEquals("XII", converter.toRoman(12));
    }

    @Test
    public void converter_returns_XIII_given_13() {
        assertEquals("XIII", converter.toRoman(13));
    }

    @Test
    public void converter_returns_XIV_given_14() {
        assertEquals("XIV", converter.toRoman(14));
    }

    @Test
    public void converter_returns_XV_given_15() {
        assertEquals("XV", converter.toRoman(15));
    }

    @Test
    public void converter_returns_XVI_given_16() {
        assertEquals("XVI", converter.toRoman(16));
    }

    @Test
    public void converter_returns_XVII_given_17() {
        assertEquals("XVII", converter.toRoman(17));
    }

    @Test
    public void converter_returns_XVIII_given_18() {
        assertEquals("XVIII", converter.toRoman(18));
    }

    @Test
    public void converter_returns_XIX_given_19() {
        assertEquals("XIX", converter.toRoman(19));
    }

    @Test
    public void converter_returns_XX_given_20() {
        assertEquals("XX", converter.toRoman(20));
    }

    @Test
    public void converter_returns_XXI_given_21() {
        assertEquals("XXI", converter.toRoman(21));
    }

    @Test
    public void converter_returns_XXIV_given_24() {
        assertEquals("XXIV", converter.toRoman(24));
    }

    @Test
    public void converter_returns_XXV_given_25() {
        assertEquals("XXV", converter.toRoman(25));
    }

    @Test
    public void converter_returns_XXVI_given_26() {
        assertEquals("XXVI", converter.toRoman(26));
    }

    @Test
    public void converter_returns_XXIX_given_29() {
        assertEquals("XXIX", converter.toRoman(29));
    }

    @Test
    public void converter_returns_XXX_given_30() {
        assertEquals("XXX", converter.toRoman(30));
    }

    @Test
    public void converter_returns_L_given_50() {
        assertEquals("L", converter.toRoman(50));
    }

    @Test
    public void converter_returns_XL_given_40() {
        assertEquals("XL", converter.toRoman(40));
    }

    @Test
    public void converter_returns_LX_given_60() {
        assertEquals("LX", converter.toRoman(60));
    }

    @Test
    public void converter_returns_C_given_100() {
        assertEquals("C", converter.toRoman(100));
    }

    @Test
    public void converter_returns_XC_given_90() {
        assertEquals("XC", converter.toRoman(90));
    }

    @Test
    public void converter_returns_XLIX_given_41() {
        assertEquals("XLI", converter.toRoman(41));
    }

    @Test
    public void converter_returns_XLV_given_45() {
        assertEquals("XLV", converter.toRoman(45));
    }

    @Test
    public void converter_returns_XLIX_given_49() {
        assertEquals("XLIX", converter.toRoman(49));
    }

    @Test
    public void converter_returns_LIX_given_59() {
        assertEquals("LIX", converter.toRoman(59));
    }

    @Test
    public void converter_returns_LXIX_given_69() {
        assertEquals("LXIX", converter.toRoman(69));
    }

    @Test
    public void converter_returns_XCIX_given_99() {
        assertEquals("XCIX", converter.toRoman(99));
    }

    @Test
    public void converter_returns_CI_given_101() {
        assertEquals("CI", converter.toRoman(101));
    }

    @Test
    public void converter_returns_CC_given_200() {
        assertEquals("CC", converter.toRoman(200));
    }

    @Test
    public void converter_returns_D_given_500() {
        assertEquals("D", converter.toRoman(500));
    }

    @Test
    public void converter_returns_CD_given_400() {
        assertEquals("CD", converter.toRoman(400));
    }

    @Test
    public void converter_returns_DCCCXLV_given_845() {
        assertEquals("DCCCXLV", converter.toRoman(845));
    }

    @Test
    public void converter_returns_M_given_1000() {
        assertEquals("M", converter.toRoman(1000));
    }

    @Test
    public void converter_returns_CM_given_900() {
        assertEquals("CM", converter.toRoman(900));
    }

    @Test
    public void converter_returns_CMXCIX_given_999() {
        assertEquals("CMXCIX", converter.toRoman(999));
    }

    @Test
    public void converter_returns_MI_given_1001() {
        assertEquals("MI", converter.toRoman(1001));
    }

    @Test
    public void converter_returns_MMXXII_given_2022() {
        assertEquals("MMXXII", converter.toRoman(2022));
    }

    @Test
    public void converter_returns_MDCCCLXXXIX_given_1889() {
        assertEquals("MDCCCLXXXIX", converter.toRoman(1889));
    }

    @Test
    public void converter_returns_MDLXXV_given_1575() {
        assertEquals("MDLXXV", converter.toRoman(1575));
    }

}
